# War Thunder Uptiers

Input your WT matches, get statistics.

Uses `localStorage` to store data, so it's currently tied to your device/browser.

## Milestones

* ~~Base functionality~~
* UI redesign
* Clean up the `localStorage` code

## Enhancements

* Automatically pull game info (API, scraping web match history)
    * Could try and pull data from the live game thing that runs on localhost and run the planes in that (if they're specified) against a list of their BRs?
* Syncing/database instead of localStorage only
* Export/import functionality
    * CSV export
    * JSON export
* Sortable table
* Sharing of data (related to export/import?)
* ~~Always display decimal point in number inputs~~ (Not using number inputs currently)
* Pre-populate/cache the existing data to save the time it takes to run the function
* Real-time validation of data (i.e., you can't have 3.7 as your BR followed by enemy BR of 8.7, as that's not possible)
    * i.e., you can't have your BR as 3.7 and then enemy BR as 8.7
    * ~~Also no BRs of 1.4, 2.9 etc. - should be possible via pattern matching~~
    * Automatic, variable stepping (1.3 -> 1.7 -> 2.0 -> 2.3)
