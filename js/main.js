const form       = document.querySelector('.form--uptier'),
      formSubmit = form.querySelector('[type="submit"]');

ownBR = document.querySelector('#own-br');
maxBR = document.querySelector('#highest-br');
minBR = document.querySelector('#lowest-br');
map   = document.querySelector('#map');
notes = document.querySelector('#notes');

form.addEventListener('submit', function(e){
	// Put data into object
	uptier_obj = {
		"ownBR": toNum(ownBR.value, 1),
		"maxBR": toNum(maxBR.value, 1),
		"minBR": toNum(minBR.value, 1),
		"map": map.value,
		"notes": notes.value
	};

	// Add object to the array in localStorage
	uptiers = localStorage.getItem('uptiers_array') ? localStorage.getItem('uptiers_array') : '[]';
	uptiers = JSON.parse(uptiers);
	uptiers.push(uptier_obj);
	uptiers = JSON.stringify(uptiers);
	localStorage.setItem('uptiers_array', uptiers);

	// TODO: add error checking
	// TODO: add current date

	resetFormValues(form);

	e.preventDefault();
});

const table = document.querySelector('.data--uptiers'),
      tbody = table.querySelector('tbody');

// Convert this to function so it can be fired after form.submit
uptiers = localStorage.getItem('uptiers_array') || [];
uptiers = JSON.parse(uptiers);
for (i = uptiers.length - 1; i >= 0; i--) {
	brDiff = parseInt(uptiers[i].maxBR * 10) - parseInt(uptiers[i].ownBR * 10);

	// TODO: consolidate into loop
	dataRow = document.createElement('tr');
	dataRow.classList.add('br-difference--' + brDiff);


	td = document.createElement('td');
	tdVal = document.createTextNode(uptiers[i].ownBR);
	td.appendChild(tdVal);
	dataRow.appendChild(td);

	td = document.createElement('td');
	tdVal = document.createTextNode(uptiers[i].maxBR);
	td.appendChild(tdVal);
	dataRow.appendChild(td);

	td = document.createElement('td');
	tdVal = document.createTextNode(uptiers[i].minBR);
	td.appendChild(tdVal);
	dataRow.appendChild(td);

	td = document.createElement('td');
	tdVal = document.createTextNode(uptiers[i].map);
	td.appendChild(tdVal);
	dataRow.appendChild(td);

	td = document.createElement('td');
	tdVal = document.createTextNode(uptiers[i].notes);
	td.appendChild(tdVal);
	dataRow.appendChild(td);

	tbody.appendChild(dataRow);
}

// Return a number to `dp` decimal places
function toNum (num, dp) {
	dp = dp || 0;

	return parseFloat(Math.round(num * Math.pow(10, dp)) / Math.pow(10,dp)).toFixed(dp);
}
// Reset input values
function resetFormValues(form) {
	inputs = form.querySelectorAll('input, textarea, select');

	i = 0;
	inputs.forEach(function(){
		inputs[i].value = '';
		i++;
	});
}