module.exports = {
	dist: {
		options: {
			outputStyle: 'compressed',
			sourceMap: false
		},
		files: {
			'../css/main.css': 'scss/styles.scss'
		}
	}
};