module.exports = {
	options: {
		processors: [
			require('autoprefixer')({
				browsers: ['last 4 versions', 'ie 11']
			}) 
		]
	},
	dist: {
		src: '../*.css'
	}
};