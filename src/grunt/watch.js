module.exports = {
	grunt: {
		files: ['Gruntfile.js']
	},
	sass: {
		files: 'scss/**/*.scss',
		tasks: ['sass','postcss:dist'],
		options: {
			livereload: true
		}
	}
};